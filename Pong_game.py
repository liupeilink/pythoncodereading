# --------------------------------------------------------------------------- #
# RUN THIS PROGRAM AT : http://www.codeskulptor.org/#user4-uNndJiBOPS7GWqc.py #
# --------------------------------------------------------------------------- #

# Implementation of classic arcade game Pong
import simplegui
import random

# initialize globals - pos and vel encode vertical info for paddles
WIDTH = 600
HEIGHT = 400       
BALL_RADIUS = 20
PAD_WIDTH = 8
PAD_HEIGHT = 80
HALF_PAD_WIDTH = PAD_WIDTH / 2
HALF_PAD_HEIGHT = PAD_HEIGHT / 2

# helper function that spawns a ball, returns a position vector and a velocity vector
# if right is True, spawn to the right, else spawn to the left
def ball_init(right):
    global ball_pos, ball_vel # these are vectors stored as lists
    ball_vel = [random.randrange(120/60, 240/60),random.randrange(60/60, 180/60)]
    if right == True :
        ball_pos = [WIDTH/2 + BALL_RADIUS , HEIGHT/2] 
    else :
        ball_pos = [WIDTH/2 - BALL_RADIUS, HEIGHT/2]
        ball_vel[0] = -ball_vel[0]

# define event handlers
def init():
    global paddle1_pos, paddle2_pos, paddle1_vel, paddle2_vel  # these are floats
    global score1, score2  # these are ints
    paddle1_pos = [HALF_PAD_WIDTH, HEIGHT/2]    
    paddle2_pos = [WIDTH-HALF_PAD_WIDTH, HEIGHT/2]
    paddle1_vel = 0
    paddle2_vel = 0
    score1 = 0 
    score2 = 0
    rand = random.randrange(2)
    if  rand == 0 :
        ball_init(True)
    else :
        ball_init(False)

def draw(c):
    global score1, score2, paddle1_pos, paddle2_pos, ball_pos, ball_vel, paddle1_vel, paddle2_vel, right   	
 
    # update paddle's vertical position, keep paddle on the screen
    if (paddle1_pos[1] > HALF_PAD_HEIGHT and 
        paddle1_pos[1] < HEIGHT - HALF_PAD_HEIGHT ) :
        paddle1_pos[1] += paddle1_vel
    elif (paddle1_pos[1] <= HALF_PAD_HEIGHT) :
        paddle1_pos[1] = HALF_PAD_HEIGHT + 1
    elif (paddle1_pos[1] >= HEIGHT - HALF_PAD_HEIGHT) :
        paddle1_pos[1] = HEIGHT - HALF_PAD_HEIGHT - 1
    paddle1_vel = 0

    
    if (paddle2_pos[1] > HALF_PAD_HEIGHT and 
        paddle2_pos[1] < HEIGHT - HALF_PAD_HEIGHT ) :
        paddle2_pos[1] += paddle2_vel
    elif (paddle2_pos[1] <= HALF_PAD_HEIGHT) :
        paddle2_pos[1] = HALF_PAD_HEIGHT + 1
    elif (paddle2_pos[1] >= HEIGHT - HALF_PAD_HEIGHT) :
        paddle2_pos[1] = HEIGHT - HALF_PAD_HEIGHT - 1
    paddle2_vel = 0
        
    # draw mid line and gutters
    c.draw_text(str(score1), (130,100), 40, "White")
    c.draw_text(str(score2), (430,100), 40, "White")

    c.draw_line([WIDTH / 2, 0],[WIDTH / 2, HEIGHT], 1, "White")
    c.draw_line([PAD_WIDTH, 0],[PAD_WIDTH, HEIGHT], 1, "White")
    c.draw_line([WIDTH - PAD_WIDTH, 0],[WIDTH - PAD_WIDTH, HEIGHT], 1, "White")
    # draw paddles
    c.draw_polygon([[paddle1_pos[0] - HALF_PAD_WIDTH, 
                    paddle1_pos[1] - HALF_PAD_HEIGHT],
                    [paddle1_pos[0] + HALF_PAD_WIDTH, 
                    paddle1_pos[1] - HALF_PAD_HEIGHT],
                    [paddle1_pos[0] + HALF_PAD_WIDTH, 
                    paddle1_pos[1] + HALF_PAD_HEIGHT],
                    [paddle1_pos[0] - HALF_PAD_WIDTH, 
                    paddle1_pos[1] + HALF_PAD_HEIGHT]], 
                    1, "Black", "White")
    
    c.draw_polygon([[paddle2_pos[0] - HALF_PAD_WIDTH, 
                    paddle2_pos[1] - HALF_PAD_HEIGHT],
                    [paddle2_pos[0] + HALF_PAD_WIDTH, 
                    paddle2_pos[1] - HALF_PAD_HEIGHT],
                    [paddle2_pos[0] + HALF_PAD_WIDTH, 
                    paddle2_pos[1] + HALF_PAD_HEIGHT],
                    [paddle2_pos[0] - HALF_PAD_WIDTH, 
                    paddle2_pos[1] + HALF_PAD_HEIGHT]], 
                    1, "Black", "White")
     
    # update ball
    
    ball_pos[0] += ball_vel[0]
    ball_pos[1] -= ball_vel[1]

    if (ball_pos[1] < BALL_RADIUS or 
        ball_pos[1] > HEIGHT - BALL_RADIUS):
        ball_vel[1] = -ball_vel[1]
        ball_pos[1] -= ball_vel[1]

    if ((ball_pos[1] > paddle1_pos[1] - HALF_PAD_HEIGHT and
        ball_pos[1] < paddle1_pos[1] + HALF_PAD_HEIGHT and
        ball_pos[0] < BALL_RADIUS + PAD_WIDTH)	or
        (ball_pos[1] > paddle2_pos[1] - HALF_PAD_HEIGHT and
        ball_pos[1] < paddle2_pos[1] + HALF_PAD_HEIGHT and
        ball_pos[0] > WIDTH - BALL_RADIUS - PAD_WIDTH)) :
            ball_vel[0] = -ball_vel[0]
            ball_pos[0] += ball_vel[0]
            ball_vel[0] = ball_vel[0] + 0.1 * ball_vel[0]
            ball_vel[1] = ball_vel[1] + 0.1 * ball_vel[1]
    
    if ball_pos[0] < BALL_RADIUS + PAD_WIDTH :
        score2 = score2 + 1;
        ball_init(True)

    elif ball_pos[0] > WIDTH - BALL_RADIUS - PAD_WIDTH:
        score1 = score1 + 1;
        ball_init(False)
    # draw ball and scores
    c.draw_circle(ball_pos, BALL_RADIUS, 2, "Black", "White")
        
def keydown(key):
    global paddle1_vel, paddle2_vel
    if key==simplegui.KEY_MAP["s"]:
        paddle1_vel += 20
    elif key==simplegui.KEY_MAP["down"]:
        paddle2_vel += 20
   
def keyup(key):
    global paddle1_vel, paddle2_vel
    if key==simplegui.KEY_MAP["w"]:
        paddle1_vel -= 20
    elif key==simplegui.KEY_MAP["up"]:
        paddle2_vel -= 20

# create frame
frame = simplegui.create_frame("Pong", WIDTH, HEIGHT)
frame.set_draw_handler(draw)
frame.set_keydown_handler(keydown)
frame.set_keyup_handler(keyup)
frame.add_button("Restart", init, 100)

# start frame
init()
frame.start()