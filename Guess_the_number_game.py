# --------------------------------------------------------------------------- #
# RUN THIS PROGRAM AT : http://www.codeskulptor.org/#user3-YnmJ3Na6UJgJIgp.py #
# --------------------------------------------------------------------------- #

# template for "Guess the number" mini-project
# input will come from buttons and an input field
# all output for the game will be printed in the console

import simplegui
import random

# initialize global variables used in your code
random_var = 0
chances_left = 7
range = 100

# define event handlers for control panel
def init():
    print
    global range
    global chances_left
    if range == 100 :
        chances_left = 7
        print "New game, range is from 0 - 100"
        print "Number of remaining guesses is ",chances_left
    elif range == 1000 :
        chances_left = 10
        print "New game, range is from 0 - 1000"
        print "Number of remaining guesses is ",chances_left
    
def range100(): 
    # button that changes range to range [0,100) and restarts
    global random_var
    global range
    range = 100
    random_var = random.randint (0, 100)
    init()
    
def range1000():
    # button that changes range to range [0,1000) and restarts
    global random_var
    global range
    range = 1000
    random_var = random.randint (0, 1000)
    init()
    
def get_input(guess):
    # main game logic goes here	
    print
    guess = int(guess)
    global chances_left

    if chances_left > 0:
        print "Guess was",guess
        chances_left = chances_left - 1
        print "Number of remaining guesses is",chances_left

        if guess > random_var and chances_left != 0:
            print "Lower"  
        elif guess < random_var and chances_left != 0:
            print "Higher"
        elif guess == random_var and chances_left != 0:
            print ("Correct")
            init()
    if chances_left == 0:
        #print "Number of remaining guesses is",chances_left
        print ("You ran out of guesses, the number was"), random_var
        init()
    

    
# create frame
f = simplegui.create_frame("Guess_the_number",300,300)

# register event handlers for control elements
f.add_button ("Range is between 0-100", range100, 200)
f.add_button ("Range is between 0-1000", range1000, 200)
f.add_input ("Enter", get_input, 200)

# start frame
f.start()
